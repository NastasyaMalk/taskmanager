Vue.component('task', {
    props: ['id','text','count'],
    template: '<div class="list"> <h3>Mission {{id}}</h3> <textarea disabled> {{text}}</textarea> <div class="image_list"> <img  src="rd.png" width="20px" height="20px" @click="changeTask(count)"><img  src="del.webp" width="20px" height="20px" @click="removeTask(count)"></div></div>',
    data: function(){
        return {
            saveclass:'1',
            taskTxt: ''
        }
    },
    methods: {
            removeTask(count) {
              var n=this.$el.getAttribute('data-col');
              if (n=='today'){
                  app.lists.today.splice(count, 1)}
            else  if (n=='all'){
                app.lists.all.splice(count, 1)}
            else  if (n=='done'){
                    app.lists.done.splice(count, 1)}
            app.saveTasks();
          },
          changeTask(count) { 
            console.log(this); 
            this.taskTxt=this.$el.childNodes[2].value; 
            if ((this.saveclass=='1')&&(!app.isEdit)) { 
            app.isEdit=!app.isEdit; 
            this.$el.childNodes[2].classList.toggle('activeText'); 
            this.$el.childNodes[2].removeAttribute('disabled'); 
            this.saveclass='2'; 
            console.log('Изменятется');
            } else if (this.saveclass=='2') { 
            app.isEdit=!app.isEdit; 
            this.$el.childNodes[2].classList.toggle('activeText'); 
            this.$el.childNodes[2].setAttribute('disabled', 'true'); 
            this.saveclass='1'; 
            console.log('НЕ Изменятется');
            } 
            
            if (this.$el.getAttribute('data-col')=='today') { 
            var add = { 
            id: app.lists.today[count].id, 
            text:  this.taskTxt
            }; 
            app.lists.today.splice(count, 1, add); 
            } else if (this.$el.getAttribute('data-col')=='all') { 
            var add = { 
            id: app.lists.all[count].id, 
            text:  this.taskTxt
            }; 
            app.lists.all.splice(count, 1, add); 
            } else if (this.$el.getAttribute('data-col')=='done') { 
            var add = { 
            id: app.lists.done[count].id, 
            text:  this.taskTxt
            }; 
            app.lists.done.splice(count, 1, add); 
            } 
            app.saveTasks(); 
            } 
}
            

})

var app = new Vue({
    el: '#app',
    data: {
      lists: {
          today:[
              {id:1, text:'test1'},
              {id:2, text:'test2'}
            ],
            all:[
                {id:1, text:'test1'},
                {id:2, text:'test1'}
            ],
            done:[
                {id:1, text:'test1'},
                {id:2, text:'test1'}
            ],
      },
      newTask:'',
      isEdit: false,
      currentId: 1
    },
    mounted() {
        if (localStorage.getItem('lists')) {
          try {
            this.lists = JSON.parse(localStorage.getItem('lists'));
          } catch(e) {
            localStorage.removeItem('lists');
          }
        }
        if (localStorage.getItem('currentId')) {
            try {
              this.currentId = localStorage.getItem('currentId');
            } catch(e) {
              localStorage.removeItem('currentId');
            }
          }
      },
      methods:{
        addTask() {
            if (!this.newTask) { 
            return; 
            } 
            var newTask = { 
            id: this.currentId, 
            text: this.newTask 
            }; 
            this.currentId++; 
            this.lists.all.push(newTask); 
            this.newTask = ''; 
            app.saveTasks(); 
          },
          saveTasks() {
            const parsed = JSON.stringify(this.lists);
            localStorage.setItem('lists', parsed);
            localStorage.setItem('currentId', this.currentId);
          },
      }
  })